package com.scwang.refreshlayout.activity.Mine;

import android.support.annotation.NonNull;

import java.util.ArrayList;

public class Message implements Comparable<Message>
{
    private String name;
    private int scores;

    public Message(String name,int scores)
    {
        this.name = name;
        this.scores = scores;
    }

    @Override
    public int compareTo(@NonNull Message message) {
        if (this.scores>message.scores)
            return  1;
        else
            return  -1;
    }
}
